using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class AnchorVertBuffer : VertBuffer {
    
    public GameObject cylinder;

    public Vector3 start;
    public Vector3 end;
    public Vector3 scale = Vector3.one;
    public List<Dictionary<float, bool>> vertIndexEdgePairs;

    public UpdateBuffer updateBuffer;

    public NeighbourData bakedData;

    private void Update()
    {
  
    }

    public override void CreateBuffer(){

    }

    public void CreateBuf()
    {
        structSize = 3 + 3 + 3 + 3 + 3 + 2 * 6;

        mesh = cylinder.GetComponent<MeshFilter>().mesh;
        updateBuffer = GetComponent<UpdateBuffer>();

        Vector3 scale = cylinder.transform.localScale;
        Vector3 delta = start - end;
        scale.z = Vector3.Magnitude(delta) * 0.5f;
        cylinder.transform.forward = delta;

        cylinder.transform.localScale = scale;
        cylinder.transform.position = Vector3.Lerp(start, end, 0.5f);

        vertices = mesh.vertices;
        uvs = mesh.uv;
        normals = mesh.normals;

        vertCount = vertices.Length;

        _buffer = new ComputeBuffer(vertCount, structSize * sizeof(float));
        values = new float[structSize * vertCount];

        int index = 0;

        Vector3 t1;
        Vector3 t2;
        int bakedNeighbourDataIndex = 0;

        for (int i = 0; i < vertCount; i++)
        {
            
            t1 = transform.TransformPoint(vertices[i]);
            t2 = transform.TransformPoint(normals[i]);
                
            // positions
            values[index++] = t1.x;
            values[index++] = t1.y;
            values[index++] = t1.z;

            // vel
            values[index++] = 0;
            values[index++] = 0;
            values[index++] = 0;

            // normals
            values[index++] = t2.x;
            values[index++] = t2.y;
            values[index++] = t2.z;
                
            // target pos
            values[index++] = t1.x;
            values[index++] = t1.y;
            values[index++] = t1.z;

                // Center Pos
                Vector3 localPos = (t1 - transform.position).normalized;
            values[index++] = localPos.x;
            values[index++] = localPos.y;
            values[index++] = localPos.z;

            // Baked Neighbour Data
            for (int k = 0; k < 12; k++)
            {
                values[index++] = bakedData.neighbourData[bakedNeighbourDataIndex++];
            }
        }

        _buffer.SetData(values);

    }
}