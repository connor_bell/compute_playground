﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class GifCapture : MonoBehaviour {

    [SerializeField]
    int targetCaptureFramerate = 30;

    [SerializeField]
    float duration = 1.570795f;

    [SerializeField]
    string pngPath = "";

    [SerializeField]
    string pngPrefix = "gif";

    bool running = false;
    int frame = 0;
    int totalFrames;

	void Update ()
    {	
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Application.targetFrameRate = targetCaptureFramerate;
            Time.captureFramerate = targetCaptureFramerate;
            running = true;
            totalFrames = (int)(targetCaptureFramerate * duration);
            if (!Directory.Exists(pngPath))
            {
                Directory.CreateDirectory(pngPath);
            }
            return;
        }

        if (running)
        {
            if (frame < totalFrames)
            {
                ScreenCapture.CaptureScreenshot(pngPath + pngPrefix + "_" + frame + ".png");
                frame++;
            }
            else
            {
                Debug.Log("Done capturing frames");
            }
        }
	}
}
