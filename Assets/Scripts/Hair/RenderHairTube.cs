﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderHairTube : MonoBehaviour {

    public AnimationCurve form;
    public int formSamples = 256;

    public HairOnVertBuffer hair;
    public Material material;

    public Color color1;
    public Color color2;
    public Color color3;
    
    public int tubeWidth;
    public int tubeLength;
    public int totalVerts;

    public float topRadius = 0.01f;

    Texture2D formTexture;
    
    void Start() {

        if (hair == null)
        {
            hair = gameObject.GetComponent<HairOnVertBuffer>();
        }

        formTexture = new Texture2D(formSamples, 1);
        Color[] vals = new Color[formSamples];

        for (int i = 0; i < formSamples; i++)
        {
            float progress = (float) i / formSamples;
            float val = form.Evaluate(progress);
            vals[i] = new Color(val,val,val);
        }

        formTexture.SetPixels(vals);
        formTexture.Apply();
        material = new Material( material );

        totalVerts = hair.totalHairs * tubeWidth * (tubeLength-1) * 3 * 2;
        material.SetTexture("_FormTexture", formTexture);

    }

    void OnRenderObject()
    {
        material.SetPass(0);

        material.SetBuffer("_vertBuffer", hair._buffer );
        material.SetVector("_Color1", color1);
        material.SetVector("_Color2", color2);
        material.SetVector("_Color3", color3);
        material.SetInt("_TubeWidth" , tubeWidth );
        material.SetInt("_NumVertsPerHair" , hair.numVertsPerHair );
        material.SetInt("_TubeLength" , tubeLength );
        material.SetInt("_TotalHair" , hair.totalHairs * hair.numVertsPerHair  );
        material.SetInt("_TotalVerts" , totalVerts );
        material.SetFloat("_TopRadius", topRadius);
        Graphics.DrawProcedural(MeshTopology.Triangles, totalVerts );
    }
}
