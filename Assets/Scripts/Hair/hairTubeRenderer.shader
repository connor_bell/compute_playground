﻿Shader "Custom/hairTubeRenderer" {
  Properties {
		    _Color1 ("Tip Color", Color) = (1,.4,.2,1)
        _Color2 ("Tail Color", Color) = (1,0,0,1)
		    _Color3 ("Tail Color", Color) = (1,0,0,1)
			_TopRadius ("Top Radius", float) = 0.05
        }

        SubShader{
          //        Tags { "RenderType"="Transparent" "Queue" = "Transparent" }
          Cull Off
          Pass{

            Lighting On
            Tags {"LightMode" = "ForwardBase"}


            CGPROGRAM

            #pragma target 4.5

            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile_fwdbase

            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
			#include "../../Shaders/Includes/trinoise.cginc"
			#include "../../Shaders/Includes/hsv.cginc"

            #include "HairVert.cginc"

            StructuredBuffer<HairVert> _vertBuffer;


            uniform int _TubeWidth;
            uniform int _NumVertsPerHair;
            uniform int _TubeLength;
            uniform int _TotalHair;
            uniform int _TotalVerts;

            uniform float4 _Color2;
            uniform float4 _Color1;
            uniform float4 _Color3;
			uniform float _TopRadius;
			uniform sampler2D _FormTexture;

        //A simple input struct for our pixel shader step containing a position.
        struct varyings {
          float4 pos      : SV_POSITION;
          float3 worldPos : TEXCOORD4;
          float3 nor      : TEXCOORD0;
          float3 eye      : TEXCOORD2;
          float3 debug    : TEXCOORD3;
          float3 col      : TEXCOORD5;
          float2 uv       : TEXCOORD1;
          LIGHTING_COORDS(6,7)
        };



        float3 cubicCurve( float t , float3  c0 , float3 c1 , float3 c2 , float3 c3 ){

          float s  = 1. - t;

          float3 v1 = c0 * ( s * s * s );
          float3 v2 = 3. * c1 * ( s * s ) * t;
          float3 v3 = 3. * c2 * s * ( t * t );
          float3 v4 = c3 * ( t * t * t );

          float3 value = v1 + v2 + v3 + v4;

          return value;

        }

		float3 cubicFromValue( in float row , in float bladeID , out float3 upPos , out float3 doPos ){

			float3 p0 = 0.;
			float3 p2 = 0.;

			float base = row * (float(_NumVertsPerHair)-1);
			float baseUp   = floor( base );
			float baseDown = ceil( base );
			float amount = row;

			int bladeBase = _NumVertsPerHair * int( bladeID );

			p0 = _vertBuffer[ int(baseUp)  + bladeBase ].pos;
			p2 = _vertBuffer[ int(baseDown)+ bladeBase ].pos;

			float3 pos = lerp( p0, p2,amount );

			upPos = lerp( p0, p2, amount + 0.1 );
			doPos = lerp( p0, p2, amount - 0.1 );

			return p2;
		}


        varyings vert (uint id : SV_VertexID){

			varyings o;

			uint tri  = id % 6;

            float bladeID = floor( id / ((_TubeWidth * (_TubeLength-1)) * 3 * 2));
            float remainID = id - bladeID * (_TubeWidth * (_TubeLength-1) * 3 * 2);
            float row = floor(remainID / (_TubeWidth * 3 * 2));
            float triID = floor( id / 6 );

			float col = triID % _TubeWidth;
		
			// Rebuilding the mesh in the vert shader

			float3 upPos; float3 doPos;

			float r1 = row / float(_TubeLength);
			float r2 = (row+1) / float(_TubeLength);

			float3 pos1 = cubicFromValue( r1 , bladeID , upPos , doPos );
			float3 nor1 = normalize( upPos - doPos );

			float3 pos2 = cubicFromValue( r2 , bladeID , upPos , doPos );
			float3 nor2 = normalize( upPos - doPos );

			float3 x1 = normalize( cross( nor1 , float3( 1, 0 , 0)));
			float3 z1 = normalize( cross( nor1 , x1 ));

			float3 x2 = normalize( cross( nor2 , float3( 1, 0 , 0)));
			float3 z2 = normalize( cross( nor2 , x2 ));

			float angle =  col/_TubeWidth * 6.28318;
			float angleU = (col+1)/_TubeWidth * 6.28318;

			float radius  = tex2Dlod(_FormTexture, fixed4(fmod(r1,1.),0., 0.,0.)).x * _TopRadius;
			float radiusU = tex2Dlod(_FormTexture, fixed4(fmod(r2,1.),0., 0.,0.)).x * _TopRadius;

			float3 f1 = radius * sin(angle ) * x1   + radius * cos( angle  ) * z1 + pos1;
			float3 f2 = radius * sin(angleU) * x1   + radius * cos( angleU ) * z1 + pos1;
			float3 f3 = radiusU * sin(angle ) * x2   + radiusU * cos( angle  ) * z2 + pos2;
			float3 f4 = radiusU * sin(angleU) * x2   + radiusU * cos( angleU ) * z2 + pos2;

			float3 n1 = normalize(f1 - pos1);
			float3 n2 = normalize(f2 - pos1);
			float3 n3 = normalize(f3 - pos2);
			float3 n4 = normalize(f4 - pos2);

			float2 uv1 = float2(r1,(col)/_TubeWidth);
			float2 uv2 = float2(r1,(col+1)/_TubeWidth);
			float2 uv3 = float2(r2,(col)/_TubeWidth);
			float2 uv4 = float2(r2,(col+1)/_TubeWidth);

			float3 finalPos; float3 finalNor; float2 finalUV;

			if( tri == 0)
			{
				finalPos = f1;
				finalNor = n1;
				finalUV = uv1;
			}
			else if( tri == 1 )
			{
				finalPos = f2;
				finalNor = n2;
				finalUV = uv2;
			}
			else if( tri == 2 )
			{
				finalPos = f4;
				finalNor = n4;
				finalUV = uv4;
			}
			else if( tri == 3 )
			{
				finalPos = f1;
				finalNor = n1;
				finalUV = uv1;
			}
			else if( tri == 4 )
			{
				finalPos = f4;
				finalNor = n4;
				finalUV = uv4;
			}
			else if( tri == 5 )
			{
				finalPos = f3;
				finalNor = n3;
				finalUV = uv3;
			}

			float3 fPos = float3( 0 , 0 , 0);

			o.worldPos = finalPos;
			o.pos = mul (UNITY_MATRIX_VP, float4(o.worldPos,1.0f));
			o.nor = UnityObjectToWorldNormal(finalNor);
			o.uv = finalUV;
			o.debug = _WorldSpaceLightPos0.xyz;
			o.eye = _WorldSpaceCameraPos - o.worldPos;

			TRANSFER_VERTEX_TO_FRAGMENT(o);

			return o;
        }

        float4 frag (varyings v) : COLOR {
			float4 c = 0.;
			float3 eye = normalize(v.worldPos-_WorldSpaceCameraPos);
			float fres = dot( v.nor, eye) + .5;

			float pNoise = triNoise3d(v.worldPos*0.25);

			c.rgb = hsv(pNoise*3., 0.25 + pNoise, fres + pNoise);
			//c.a = 1.;fres + pNoise*4.;
			c.a = 1.;
			return saturate(c);
        }

        ENDCG

      }
    }

    Fallback Off

  }
