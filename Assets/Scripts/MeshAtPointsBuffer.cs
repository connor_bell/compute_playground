﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshAtPointsBuffer : VertBuffer {

	public bool rotateMesh;
	public bool scaleMesh;
	public bool translateMesh;

	[SerializeField]
	Mesh meshAtPoint;

	[SerializeField]
	Mesh sourceMesh;
    
	[SerializeField]
	float destScale = 1.0f;

	void OnEnable () {
		CreateBuffer();
	}

	void OnDisable()
	{
		_buffer.Release();
	}

	struct Vert {
		public Vector3 pos;
		public Vector3 vel;
		public Vector3 nor;

		public Vector3 targetPos;
        public Vector3 centerPos;
        public Vector3 localPos;

    };


	public override void CreateBuffer(){

		int structSize = 3 + 3 + 3 + 3 + 3+ 3;

		if (sourceMesh == null)
		{
			sourceMesh = gameObject.GetComponent<MeshFilter>().mesh;
		}

		if (meshAtPoint == null)
		{
			meshAtPoint = sourceMesh;
		}

        Dictionary<Vector3, int> uniqueVerts = new Dictionary<Vector3, int>();

        foreach (Vector3 sourceVert in sourceMesh.vertices)
        {
            if (!uniqueVerts.ContainsKey(sourceVert))
            {
                uniqueVerts[sourceVert] = 1;
            }
        }

        vertCount = uniqueVerts.Count * meshAtPoint.vertices.Length;
        Vector3[] verts = new Vector3[uniqueVerts.Count];
        int index = 0;
        foreach (Vector3 v in uniqueVerts.Keys)
        {
                verts[index] = v;
                index++;
        }
        _buffer = new ComputeBuffer( vertCount , structSize * sizeof(float) );
		values = new float[ structSize * vertCount ];
       
		index = 0;
        foreach (Vector3 sourceVert in verts)
		{	
			Vector3 targetPosition;
			Vector3 targetNormal;

			for (int i = 0; i < meshAtPoint.vertices.Length; i++)
			{
				if (rotateMesh) 
				{
					targetPosition = transform.TransformDirection( meshAtPoint.vertices[i] + sourceVert);
					targetNormal = transform.TransformDirection( meshAtPoint.normals[i] );
				}
				else
				{
					targetPosition = meshAtPoint.vertices[i]* destScale+ sourceVert;
					targetNormal = meshAtPoint.normals[i];
				}

				if (scaleMesh)
				{
					targetPosition = transform.TransformVector( targetPosition ) ;
					targetNormal = transform.TransformVector( targetNormal );
				}

				if (translateMesh)
				{
					targetPosition = transform.TransformPoint( targetPosition );
					targetNormal = transform.TransformPoint( targetNormal );
				}
                

				// positions
				values[ index++ ] = targetPosition.x * .99f;
				values[ index++ ] = targetPosition.y * .99f;
				values[ index++ ] = targetPosition.z * .99f;

				// vel
				values[ index++ ] = 0;
				values[ index++ ] = 0;
				values[ index++ ] = 0;

				// normals
				values[ index++ ] = targetNormal.x;
				values[ index++ ] = targetNormal.y;
				values[ index++ ] = targetNormal.z;
                    
				// target pos
				values[index++] = targetPosition.x;
				values[index++] = targetPosition.y;
				values[index++] = targetPosition.z;
				
				// Center Pos
				values[index++] = sourceVert.x;
				values[index++] = sourceVert.y;
                values[index++] = sourceVert.z;

                // Local Pos
                values[index++] = meshAtPoint.vertices[i].x;
                values[index++] = meshAtPoint.vertices[i].y;
                values[index++] = meshAtPoint.vertices[i].z;
            }
		}

		_buffer.SetData(values);

	}
}