﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshAtVerts : MonoBehaviour {
    
    [SerializeField]
    ComputeShader computeShader;

    [SerializeField]
    MeshAtPointsBuffer meshAtVertBuffer;
    
    [SerializeField]
    TriangleOnMeshPointsBuffer meshAtVertTriBuffer;

    [SerializeField]
    Material renderMaterial;
    
    private int numGroups = 0;
    private int numThreads = 64;
    private int vertCount = 0;
    private int kernel;
    
    private int triangleCount;

    void OnEnable()
    {
        kernel = computeShader.FindKernel("CSMain");
    }

    void FixedUpdate()
    {
        Dispatch();
    }

    void OnRenderObject()
    {
        renderMaterial.SetPass(0);
        renderMaterial.SetBuffer("_vertBuffer", meshAtVertBuffer._buffer);
        renderMaterial.SetBuffer("_triBuffer", meshAtVertTriBuffer._buffer);
        Graphics.DrawProcedural(MeshTopology.Triangles, triangleCount);
    }

    void Dispatch()
    {
        /*
        if (leftController != null)
        {
            Vector4 p = leftController.transform.position;
            p.w = leftController.Grab;
            computeShader.SetVector("_Influencer1Position", p);
            vertMeshComputeShader.SetVector("_Influencer1Position", p);
        }

        if (rightController != null)
        {
            Vector4 p = rightController.transform.position;
            p.w = rightController.Grab;
            computeShader.SetVector("_Influencer2Position", p);
            vertMeshComputeShader.SetVector("_Influencer2Position", p);
        }
        */

        if (meshAtVertBuffer._buffer != null)
        {
            numGroups = (vertCount + (numThreads - 1)) / numThreads;
            triangleCount = meshAtVertTriBuffer.triCount;

            computeShader.SetInt("_NumVerts", meshAtVertBuffer.vertCount);
            computeShader.SetBuffer(kernel, "vertBuffer", meshAtVertBuffer._buffer);
            computeShader.Dispatch(kernel, numGroups, 1, 1);
        }
    }
}
