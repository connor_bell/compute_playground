﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshToEdge : MonoBehaviour {

    [SerializeField]
    Mesh mesh;

    [SerializeField]
    AnchorVertBuffer vertBufPrefab;

    [SerializeField]
    bool scale;

    public List<AnchorVertBuffer> instantiatedVertBuffers;

    struct Edge
    {
        public Vector3 start;
        public Vector3 end;

        public Edge (Vector3 s, Vector3 e)
        {
            start = s;
            end = e;
        }
    }

    Dictionary<Edge, bool> edgeDict = new Dictionary<Edge, bool>();

    private void OnDisable()
    {
        foreach (AnchorVertBuffer buf in instantiatedVertBuffers)
            {
                GameObject.DestroyImmediate(buf.gameObject);
            }
    }

    void OnEnable ()
    {
	    for (int i = 0; i < mesh.triangles.Length; i+=3)
        {
            Vector3 v1 = transform.position + mesh.vertices[i] * transform.localScale.x;
            Vector3 v2 = transform.position + mesh.vertices[i+1] * transform.localScale.y;
            Vector3 v3 = transform.position + mesh.vertices[i+2] * transform.localScale.z;

            Edge e1 = new Edge(v1, v2);
            Edge e2 = new Edge(v2, v3);

            Edge inverse_e1 = new Edge(v2, v1);
            Edge inverse_e2 = new Edge(v3, v2);

            if ((!edgeDict.ContainsKey(e1) && !edgeDict.ContainsKey(inverse_e1)))
            {
                edgeDict[e1] = true;
            }
            if ((!edgeDict.ContainsKey(e2) && !edgeDict.ContainsKey(inverse_e2)))
            {
                edgeDict[e2] = true;
            }
        }

        foreach (Edge edge in edgeDict.Keys)
        {
            AnchorVertBuffer buf = GameObject.Instantiate(vertBufPrefab);
            buf.gameObject.SetActive(true);
            buf.start = edge.start;
            buf.end = edge.end;
            if (scale)
            {
                buf.scale = transform.localScale;
            }
            buf.CreateBuf();
            instantiatedVertBuffers.Add(buf);
                buf.transform.parent = transform;
        }
    }
}