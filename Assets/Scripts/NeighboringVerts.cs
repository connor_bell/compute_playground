﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class NeighboringVerts : MonoBehaviour {

    [SerializeField]
    Mesh mesh;

    [SerializeField]
    string savePath = "Assets/";

    [SerializeField]
    string filename = "NeighbourData.asset";

    [SerializeField]
    int numNeighbours = 8;

    public Dictionary<int, Dictionary<int, bool>> vertIndexEdgePairs;
    private int count = 0;
    
    [ContextMenu("Compute")]
    void Compute()
    {
        NeighbourData obj = ScriptableObject.CreateInstance<NeighbourData>();
        CalculateNeighbors();
        obj.neighbourData = new float[mesh.vertices.Length * numNeighbours];
        obj.numNeighbours = numNeighbours;

        int index = 0;
        for (int i = 0; i < mesh.vertices.Length; i++)
        {
            if (vertIndexEdgePairs.ContainsKey(i))
            {
                Dictionary<int, bool> nVerts = vertIndexEdgePairs[i];
                foreach (KeyValuePair<int, bool> pair in nVerts)
                {
                    obj.neighbourData[index++] = pair.Key;
                }

                // Fill in the remaining slots for neighbours with -1 which is used in the shader to zero-out the neighbour velocity
                for (int n = 0; n < numNeighbours - nVerts.Count; n++)
                {
                    obj.neighbourData[index++] = -1;
                }
            }
            else
            {
                for (int n = 0; n < numNeighbours; n++)
                {
                    obj.neighbourData[index++] = -1;
                }
            }
        }

        AssetDatabase.CreateAsset(obj, savePath + filename);
        AssetDatabase.SaveAssets();
    }
    
    public void CalculateNeighbors()
    {
        mesh = GetComponent<MeshFilter>().sharedMesh;

        vertIndexEdgePairs = new Dictionary<int, Dictionary<int, bool>>();

        // Store a list of keys for each position to update if there are duplicate verts
        Dictionary<Vector3, List<int>> positions = new Dictionary<Vector3, List<int>>();

        for (int i = 0; i < mesh.vertices.Length; i++)
        {
            Dictionary<int, bool> verts;
            List<int> updateIndices = new List<int>();
            
            if (positions.ContainsKey(mesh.vertices[i]) && positions[mesh.vertices[i]].Count > 0)
            {
                // Just using the first int as a key should be fine since we update all of the edge pairs
                verts = vertIndexEdgePairs[positions[mesh.vertices[i]][0]];
                updateIndices = positions[mesh.vertices[i]];
            }
            else
            {
                verts = new Dictionary<int, bool>();
            }

            // Search thru the triangles of the mesh to find triangles containing the current vert, and add other members in triangle as neighbouring verts.
            for (int t = 0; t < mesh.triangles.Length; t += 3)
            {
                if (verts.Count >= numNeighbours) break;

                int v1 = mesh.triangles[t];
                int v2 = mesh.triangles[t + 1];
                int v3 = mesh.triangles[t + 2];

                if (v1 == i)
                {
                    verts[v2] = false;
                    if (verts.Count >= numNeighbours) break;
                    verts[v3] = false;
                }

                if (verts.Count >= numNeighbours) break;

                if (v2 == i)
                {
                    verts[v1] = false;
                    if (verts.Count >= numNeighbours) break;
                    verts[v3] = false;
                }

                if (verts.Count >= numNeighbours) break;

                if (v3 == i)
                {
                    verts[v1] = false;
                    if (verts.Count >= numNeighbours) break;
                    verts[v2] = false;
                }
            }

            updateIndices.Add(i);
            positions[mesh.vertices[i]] = new List<int>(updateIndices);
            vertIndexEdgePairs[i] = verts;
            
            if (updateIndices.Count > 1)
            {
                foreach (int index in updateIndices)
                {
                    vertIndexEdgePairs[index] = new Dictionary<int, bool>(verts);
                }
            }
        }
    }
}
