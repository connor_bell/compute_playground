﻿using UnityEngine;

public class NeighbourData : ScriptableObject
{
    [SerializeField]
    public float[] neighbourData;

    [SerializeField]
    public int numNeighbours;
}