using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeighbourVertBuffer : VertBuffer {
    
    public NeighbourData bakedData;
    
    public override void CreateBuffer()
    {
        structSize = 3 + 3 + 3 + 3 + bakedData.numNeighbours;

        vertices = mesh.vertices;
        uvs = mesh.uv;
        normals = mesh.normals;

        vertCount = vertices.Length;

        _buffer = new ComputeBuffer(vertCount, structSize * sizeof(float));
        values = new float[structSize * vertCount];

        int index = 0;

        Vector3 t1;
        Vector3 t2;
        int bakedNeighbourDataIndex = 0;

        for (int i = 0; i < vertCount; i++)
        {

            t1 = transform.TransformPoint(vertices[i]);
            t2 = normals[i];

            // positions
            values[index++] = t1.x;
            values[index++] = t1.y;
            values[index++] = t1.z;

            // vel
            values[index++] = 0;
            values[index++] = 0;
            values[index++] = 0;

            // normals
            values[index++] = t2.x;
            values[index++] = t2.y;
            values[index++] = t2.z;

            // target pos
            values[index++] = t1.x;
            values[index++] = t1.y;
            values[index++] = t1.z;
            
            // Baked Neighbour Data
            for (int k = 0; k < 11; k++)
            {
                values[index++] = bakedData.neighbourData[bakedNeighbourDataIndex++];
            }
        }

        _buffer.SetData(values);
    }
}