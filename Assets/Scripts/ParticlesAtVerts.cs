﻿using UnityEngine;

// Put a particle at each point on a mesh
public class ParticlesAtVerts : MonoBehaviour {

    [SerializeField]
    ComputeShader computeShader;
    
    [SerializeField]
    Material renderMaterial;

    [SerializeField]
    VertBuffer vertBuffer;

    [SerializeField]
    FFTBuffer fft;
    
    private int numGroups = 0;
    private int numThreads = 64;
    private int vertCount = 0;
    private int particleKernel;
    
    private float currentTime = 0f;
    private int triangleCount;

    void OnEnable()
    {
        particleKernel = computeShader.FindKernel("CSMain");
    }

    void FixedUpdate()
    {
        Dispatch();
    }

    void OnRenderObject()
    {
        renderMaterial.SetPass(0);
        
        renderMaterial.SetBuffer("_vertBuffer", vertBuffer._buffer);
        renderMaterial.SetBuffer("_FFT", fft._buffer);

        Graphics.DrawProcedural(MeshTopology.Triangles, vertCount * 6);
    }

    void Dispatch()
    {
        /*
        if (leftController != null)
        {
            Vector4 p = leftController.transform.position;
            p.w = leftController.Grab;
            computeShader.SetVector("_Influencer1Position", p);
            vertMeshComputeShader.SetVector("_Influencer1Position", p);
        }

        if (rightController != null)
        {
            Vector4 p = rightController.transform.position;
            p.w = rightController.Grab;
            computeShader.SetVector("_Influencer2Position", p);
            vertMeshComputeShader.SetVector("_Influencer2Position", p);
        }
        */

        if (vertBuffer._buffer != null)
        {
            vertCount = vertBuffer.vertCount;
            numGroups = (vertCount + (numThreads - 1)) / numThreads;

            computeShader.SetInt("_NumVerts", vertBuffer.vertCount);
            computeShader.SetBuffer(particleKernel, "vertBuffer", vertBuffer._buffer);
            computeShader.Dispatch(particleKernel, numGroups, 1, 1);
        }
    }
}