﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CBOctahedronTrip
{
    public class CBRenderTubeBuffer : MonoBehaviour
    {

        [SerializeField]
        ComputeShader computeShader;

        [SerializeField]
        Material renderMaterial;

        [SerializeField]
        CBTubeBuffer tubeBuffer;

        private int numGroups = 0;
        private int numThreads = 64;
        private int kernel;
        private int vertCount = 0;
        private int triangleCount = 0;

        void OnEnable()
        {
            //        kernel = computeShader.FindKernel("CSMain");
        }

        void FixedUpdate()
        {
            Dispatch();
        }

        void OnRenderObject()
        {
            renderMaterial.SetPass(0);
            renderMaterial.SetBuffer("_vertBuffer", tubeBuffer._vertBuffer);
            renderMaterial.SetBuffer("_triBuffer", tubeBuffer._triBuffer);
            Graphics.DrawProcedural(MeshTopology.Triangles, tubeBuffer.triCount);
        }

        void Dispatch()
        {
            return;
            if (tubeBuffer._vertBuffer != null)
            {
                vertCount = tubeBuffer.vertCount;
                numGroups = (vertCount + (numThreads - 1)) / numThreads;
                triangleCount = tubeBuffer.triCount;

                computeShader.SetInt("_NumVerts", tubeBuffer.vertCount);
                computeShader.SetBuffer(kernel, "vertBuffer", tubeBuffer._vertBuffer);
                computeShader.Dispatch(kernel, numGroups, 1, 1);
            }

        }
    }
}