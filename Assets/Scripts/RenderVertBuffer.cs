﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RenderVertBuffer : MonoBehaviour {

    [SerializeField]
    Material renderMaterial;
    
    VertBuffer vertBuffer;
    TriangleBuffer triangleBuffer;

    void Start () {
        vertBuffer = GetComponent<VertBuffer>();
        triangleBuffer = GetComponent<TriangleBuffer>();
    }

    void OnRenderObject()
    {
        renderMaterial.SetPass(0);
        renderMaterial.SetBuffer("_vertBuffer", vertBuffer._buffer);
        renderMaterial.SetBuffer("_triBuffer", triangleBuffer._buffer);

        Graphics.DrawProcedural(MeshTopology.Triangles, triangleBuffer.triCount);
    }
}
