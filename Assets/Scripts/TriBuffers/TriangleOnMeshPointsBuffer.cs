﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TriangleOnMeshPointsBuffer : MonoBehaviour
{
    public int triCount;
    public ComputeBuffer _buffer;
    public int[] triangles;

    [SerializeField]
    Mesh meshAtPoint;

    [SerializeField]
    Mesh sourceMesh;
        
    void Awake()
    {
        GetMesh();
        CreateBuffer();
    }

    void OnDisable()
    {
        ReleaseBuffer();
    }

    public void ReleaseBuffer()
    {
        _buffer.Release();
    }

    public virtual void GetMesh()
    {

    }

    public virtual void CreateBuffer()
    {
        if (sourceMesh == null)
        {
            sourceMesh = gameObject.GetComponent<MeshFilter>().mesh;
        }

        if (meshAtPoint == null)
        {
            meshAtPoint = sourceMesh;
        }

        triCount = meshAtPoint.triangles.Length * sourceMesh.vertices.Length;

        int index = 0;

        List<int> tris = new List<int>();
        foreach (Vector3 sourceVert in sourceMesh.vertices)
        {
            int offset = index * meshAtPoint.triangles.Length;
            foreach (int tri in meshAtPoint.triangles)
            {
                tris.Add(tri + offset);
            }
            index++;
        }

        triangles = tris.ToArray();

        _buffer = new ComputeBuffer(triCount, sizeof(int));
        _buffer.SetData(triangles);
    }
}
