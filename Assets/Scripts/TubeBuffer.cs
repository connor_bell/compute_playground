﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CBOctahedronTrip
{
    public class CBTubeBuffer : VertTriBuffer
    {

        public Transform start;
        public Transform end;

        [SerializeField]
        float radius = 0.25f;

        [SerializeField]
        int radialSegments = 16;

        [SerializeField]
        int depthSegments = 8;

        struct Vert
        {
            public float used;
            public Vector3 pos;
            public Vector3 vel;
            public Vector3 nor;
            public Vector2 uv;

            public Vector3 targetPos;
            public Vector3 debug;
        };

        public override void CreateBuffer()
        {

            int structSize = 1 + 3 + 3 + 3 + 2 + 3 + 3;

            vertCount = depthSegments * radialSegments;
            vertices = new Vector3[vertCount];
            triCount = (depthSegments - 1) * (radialSegments - 1) * 6;
            _vertBuffer = new ComputeBuffer(vertCount, structSize * sizeof(float));
            _triBuffer = new ComputeBuffer(triCount, triCount * sizeof(int));

            vertValues = new float[structSize * vertCount];
            triValues = new float[triCount];

            int index = 0;
            int triIndex = 0;
            Vector3 tubeDir = end.position - start.position;
            Vector3 originalForward = start.forward;
            start.forward = tubeDir;

            Vector3 tubeUp = start.up;
            Vector3 tubeRight = Vector3.Cross(tubeDir, tubeUp).normalized;
            start.forward = originalForward;

            int vertIndex = 0;
            for (int i = 0; i < depthSegments; i++)
            {
                Vector3 targetPosition;
                Vector3 targetNormal;
                float depthProgress = ((float)i / (float)depthSegments);

                Vector3 centerTubePos = Vector3.Lerp(start.position, end.position, depthProgress);

                for (int j = 0; j < radialSegments; j++)
                {
                    float radialProgress = ((float)j / (float)radialSegments);

                    targetPosition = centerTubePos + tubeRight * Mathf.Cos(radialProgress * Mathf.PI * 2.0f) * radius + tubeUp * Mathf.Sin(radialProgress * Mathf.PI * 2.0f) * radius;
                    vertices[vertIndex] = targetPosition;

                    targetNormal = Vector3.Normalize(targetPosition - centerTubePos);

                    // used 
                    vertValues[index++] = 1;

                    // positions
                    vertValues[index++] = targetPosition.x * .99f;
                    vertValues[index++] = targetPosition.y * .99f;
                    vertValues[index++] = targetPosition.z * .99f;

                    // vel
                    vertValues[index++] = 0;
                    vertValues[index++] = 0;
                    vertValues[index++] = 0;

                    // normals
                    vertValues[index++] = targetNormal.x;
                    vertValues[index++] = targetNormal.y;
                    vertValues[index++] = targetNormal.z;

                    // uvs
                    vertValues[index++] = radialProgress;
                    vertValues[index++] = depthProgress;

                    vertValues[index++] = targetPosition.x;
                    vertValues[index++] = targetPosition.y;
                    vertValues[index++] = targetPosition.z;

                    // Debug
                    vertValues[index++] = targetNormal.x;
                    vertValues[index++] = targetNormal.y;
                    vertValues[index++] = targetNormal.z;

                    vertIndex++;
                }
            }

            // Triangles
            for (int i = 0; i < depthSegments - 1; i++)
            {
                for (int j = 0; j < radialSegments - 1; j++)
                {
                    int baseIndex = i * radialSegments + j;

                    triValues[triIndex++] = baseIndex + radialSegments + 1;
                    triValues[triIndex++] = baseIndex + (radialSegments);
                    triValues[triIndex++] = baseIndex;

                    triValues[triIndex++] = baseIndex + (radialSegments) + 1;
                    triValues[triIndex++] = (baseIndex + 1);
                    triValues[triIndex++] = (baseIndex);
                }
            }
            _vertBuffer.SetData(vertValues);
            _triBuffer.SetData(triValues);
        }
    }
}