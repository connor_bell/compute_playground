﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateBuffer : MonoBehaviour
{
    [SerializeField]
    ComputeShader computeShader;

    [SerializeField]
    VertBuffer vertBuffer;

    [SerializeField]
    Transform c1;

    [SerializeField]
    Transform c2;

    [SerializeField]
    float springStiffness = 0.005f;

    [SerializeField]
    float springLength = 0.025f;

    [SerializeField]
    float springDamping = 0.025f;

    [SerializeField]
    float forceMultiplier = 0.25f;

    [SerializeField]
    float targetPosForceMultiplier = 0.25f;

    [SerializeField]
    float neighbourForcesMultiplier = 0.25f;

    [SerializeField]
    float l = 0.05f;

    private int numGroups = 0;
    private int numThreads = 64;
    private int kernel;
    private int vertCount = 0;

    private int raymarchKernel;

    private Vector4 influencer1;
    private Vector4 influencer2;

    void Start()
    {
        if (vertBuffer == null)
        {
            vertBuffer = GetComponent<VertBuffer>();
        }

        raymarchKernel = computeShader.FindKernel("CSMain");
    }

    void FixedUpdate()
    {
        Dispatch();
    }
    
    void Dispatch()
    {
        computeShader.SetFloat("_Time", Time.time);
        computeShader.SetInt("_NumVerts", vertBuffer.vertCount);

        influencer1.x = c1.position.x;
        influencer1.y = c1.position.y;
        influencer1.z = c1.position.z;
        if (c2 != null)
        {
            influencer2.x = c2.position.x;
            influencer2.y = c2.position.y;
            influencer2.z = c2.position.z;
        }
        computeShader.SetVector("_Influencer1Position", influencer1);
        computeShader.SetVector("_Influencer2Position", influencer2);

        computeShader.SetFloat("_Stiffness", springStiffness);
        computeShader.SetFloat("_SpringLength", springLength);
        computeShader.SetFloat("_Damping", springDamping);
        computeShader.SetFloat("_ForceMultiplier", forceMultiplier);
        computeShader.SetFloat("_NeighbourForcesMultiplier", neighbourForcesMultiplier);
        computeShader.SetFloat("_TargetPosForceMultiplier", targetPosForceMultiplier);
        computeShader.SetFloat("_MinDistToEffector", l);

        if (vertBuffer._buffer != null)
        {
            vertCount = vertBuffer.vertCount;
            numGroups = (vertCount + (numThreads - 1)) / numThreads;

            computeShader.SetBuffer(kernel, "vertBuffer", vertBuffer._buffer);
            computeShader.Dispatch(kernel, numGroups, 1, 1);
        }
    }

    public void ActivatePinch1()
    {
        influencer1.w = 1.0f;
    }

    public void DeactivatePinch1()
    {
        influencer1.w = 0f;
    }

    public void ActivatePinch2()
    {
        influencer2.w = 1.0f;
    }

    public void DeactivatePinch2()
    {
        influencer2.w = 0f;
    }
}
