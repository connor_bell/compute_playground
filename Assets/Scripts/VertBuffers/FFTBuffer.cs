﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FFTBuffer : MonoBehaviour {

    public ComputeBuffer _buffer;
    
    [SerializeField]
    int samples = 256;
    
    private int structSize;
    float[] bufferValues;
    public float[] audioData;

    struct FFT
    {
        float data;
    }
    
    void OnEnable ()
    {
        CreateBuffer();
    }

    void OnDisable()
    {
        DestroyBuffer();
    }

    private void Update()
    {
      //  UpdateFFT();
    }

    /*
    void UpdateFFT()
    {

        tripInteraction.GetSpectrumData(audioData, 0, FFTWindow.Hanning);
        int index = 0;

        for (int i = 0; i < samples; i++)
        {
            bufferValues[index++] = audioData[i] *1000f;
        }

        _buffer.SetData(bufferValues);
    }
    */
    void CreateBuffer()
    {
        structSize = 1 + 1;
        _buffer = new ComputeBuffer(samples, structSize * sizeof(float));
        bufferValues = new float[structSize * samples];
        audioData = new float[samples];
    }

    void DestroyBuffer()
    {
        _buffer.Release();
    }
}
