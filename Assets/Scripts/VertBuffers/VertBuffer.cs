﻿using UnityEngine;
using System.Collections;

public class VertBuffer : MonoBehaviour {

    public Mesh mesh;
    public int vertCount;
    public ComputeBuffer _buffer; 
    public float[] values;

    [HideInInspector]
    public Vector3[] vertices;

    [HideInInspector]
    public Vector3[] normals;

    [HideInInspector]
    public Vector2[] uvs;

    public int structSize;

    [HideInInspector]
    public bool ready = false;

    public void OnEnable()
    {
        CreateBuffer();
        ready = true;
    }

    void OnDisable()
    {
        ReleaseBuffer();
        ready = false;
    }

    public void ReleaseBuffer()
    {
        _buffer.Release(); 
    }

    public virtual void CreateBuffer()
    {

    }
}
