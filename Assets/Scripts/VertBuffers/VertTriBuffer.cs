﻿using UnityEngine;
using System.Collections;

public class VertTriBuffer : MonoBehaviour {

    public int vertCount;
    public int triCount;
    public ComputeBuffer _vertBuffer; 
    public ComputeBuffer _triBuffer; 
  
    protected float[] vertValues;
    public float[] triValues;

    public Vector3[] vertices;

    [HideInInspector]
    public Vector3[] normals;
    
    [HideInInspector]
    public Vector2[] uvs;
  
    [HideInInspector]
    public int[] tris;

    public void Awake()
    {
        CreateBuffer();
    }

    void OnDisable(){
        ReleaseBuffer();
    }

    public void ReleaseBuffer()
    {
        _vertBuffer.Release(); 
        _triBuffer.Release(); 
    }


    public virtual void CreateBuffer()
    {
        
    }
}