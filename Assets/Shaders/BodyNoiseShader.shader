﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Unlit/CBBodyNoiseShader"
{
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"
			#include "Includes/hsv.cginc"
			#include "Includes/trinoise.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float3 worldPos : TEXCOORD1; 
				float3 worldNormal : TEXCOORD2;
				float3 localPos : TEXCOORD3;
			};

			float _Grab;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.worldPos = mul (unity_ObjectToWorld, v.vertex).xyz;
				o.worldNormal = UnityObjectToWorldNormal(v.normal);
				o.localPos = v.vertex;

				return o;
			}
	
			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 c = 1.;
				float3 eye = normalize(i.worldPos-_WorldSpaceCameraPos);
				float fres = dot( i.worldNormal, eye) + .35 + _Grab*0.15;

				float pNoise = triNoise3d(i.localPos*0.5);

				c.rgb = hsv(pNoise*3., saturate(1. - fres) - 0.2, fres + _Grab*0.25 + pNoise);
				c.a = fres + pNoise*4.;
				return c;
			}
			ENDCG
		}
	}
}
