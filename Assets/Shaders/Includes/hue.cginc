float3 hue(float3 color, float shift) {

    const float3  kRGBToYPrime = float3 (0.299, 0.587, 0.114);
    const float3  kRGBToI     = float3 (0.596, -0.275, -0.321);
    const float3  kRGBToQ     = float3 (0.212, -0.523, 0.311);

    const float3  kYIQToR   = float3 (1.0, 0.956, 0.621);
    const float3  kYIQToG   = float3 (1.0, -0.272, -0.647);
    const float3  kYIQToB   = float3 (1.0, -1.107, 1.704);

    // Convert to YIQ
    float   YPrime  = dot (color, kRGBToYPrime);
    float   I      = dot (color, kRGBToI);
    float   Q      = dot (color, kRGBToQ);

    // Calculate the hue and chroma
    float   hue     = atan2 (Q, I);
    float   chroma  = sqrt (I * I + Q * Q);

    // Make the user's adjustments
    hue += shift;

    // Convert back to YIQ
    Q = chroma * sin (hue);
    I = chroma * cos (hue);

    // Convert back to RGB
    float3    yIQ   = float3 (YPrime, I, Q);
    color.r = dot (yIQ, kYIQToR);
    color.g = dot (yIQ, kYIQToG);
    color.b = dot (yIQ, kYIQToB);

    return color;
}