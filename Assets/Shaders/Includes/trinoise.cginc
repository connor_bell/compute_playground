float tri(in float x){return abs(frac(x)-.5);}
			
float3 tri3(in float3 p){
	float tripx = tri(p.x);
	return float3( tri(p.z+tri(p.y)), tri(p.z+tripx), tri(p.y+tripx));
}
       
float triNoise3d(in float3 p)
{
	float z=1.4;
	float rz = 0.;
	float3 bp = p;
	for (float i=0.; i<=3.; i++ )
	{
		bp*=2.0;
		float3 dg = tri3(bp);
		p += (dg);

		z *= 1.5;
		p *= 1.2;
        
		rz+= (tri(p.z+tri(p.x+tri(p.y))))/z;
		bp += 0.14;
	}
	return rz;
}