
Shader "RenderProcedural/VertBufferMeshNeighbourRenderer" {
	Properties{

	}
	SubShader {
		// COLOR PASS
		Pass {
			Tags{ "LightMode" = "ForwardBase" }
			Cull Off
		
			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase 
			
			#include "UnityCG.cginc"

			#define NUM_NEIGHBOURS 11

			struct VertWithNeighbours {
				float3 pos;
				float3 vel;
				float3 nor;
				float3 targetPos;
				float neighbours[NUM_NEIGHBOURS];
			};

			StructuredBuffer<VertWithNeighbours> _vertBuffer;
			StructuredBuffer<int> _triBuffer;

			struct varyings {
				float4 pos 		: SV_POSITION;
				float3 nor 		: TEXCOORD1;
				float3 worldPos : TEXCOORD3;
			};

			varyings vert(uint id : SV_VertexID) {
				VertWithNeighbours v = _vertBuffer[_triBuffer[id]];
				float3 fPos = v.pos;
				float3 fNor = UnityObjectToWorldNormal(v.nor);

				varyings o;
				UNITY_INITIALIZE_OUTPUT(varyings,o);
				
				o.pos = mul(UNITY_MATRIX_VP, float4(fPos,1));
				o.worldPos = v.vel;
				o.nor = (v.nor);
		
				return o;
			}

			float4 frag(varyings v) : COLOR {
				float3 col = v.nor*0.5+0.5;
				return float4( col, 1.);
			}

			ENDCG
		}

	}
}