
Shader "RenderProcedural/VertBufferMeshRenderer" {
	Properties{
		_Texture( "Texture" , 2D ) = "white" {}
	}
	SubShader {
		// COLOR PASS
		Pass {
			Tags{ "LightMode" = "ForwardBase" }
			Cull Off
		
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile_fwdbase 
			
			#include "UnityCG.cginc"
			#include "AutoLight.cginc"
			 struct Vert{

			  float3 pos;
			  float3 vel;
			  float3 nor;

			  float3 targetPos;
			  float3 centerPos;
			  float3 localPos;

			};
			StructuredBuffer<Vert> _vertBuffer;
			StructuredBuffer<int> _triBuffer;
			sampler2D _Texture;
			float3 _Pos;

			struct varyings {
				float4 pos 		: SV_POSITION;
				float3 nor 		: TEXCOORD1;
			};

			varyings vert(uint id : SV_VertexID) {
				Vert v = _vertBuffer[_triBuffer[id]];
				varyings o;
				UNITY_INITIALIZE_OUTPUT(varyings,o);
				
				o.pos = mul(UNITY_MATRIX_VP, float4(v.pos,1));
				o.nor =  normalize(v.centerPos) + normalize(v.localPos)*0.35;
				
				return o;

			}
			float rgb2hue(float3 c)
{
				float4 K = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
				float4 p = lerp(float4(c.bg, K.wz), float4(c.gb, K.xy), step(c.b, c.g));
				float4 q = lerp(float4(p.xyw, c.r), float4(c.r, p.yzx), step(p.x, c.r));

				float d = q.x - min(q.w, q.y);
				float e = 1.0e-10;
				return abs(q.z + (q.w - q.y) / (6.0 * d + e));
			}
			float4 frag(varyings v) : COLOR {
				float3 col = v.nor*0.5+0.5;
				float h = rgb2hue(col);
				col = tex2D(_Texture, float2(h, 0.));
				return float4(col, 1.);
			}

			ENDCG
		}

	}
}