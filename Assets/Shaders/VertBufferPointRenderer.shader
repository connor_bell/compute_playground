﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "RenderProcedural/VertBufferPointRenderer" {

	Properties {
  		  _Texture( "Texture" , 2D ) = "white" {}
        _Color("Color", Color) = (1,.4,.2,1)
        _Size("Size", float) = 0.0125
    }




  SubShader{
    Cull off
	Tags {  "RenderType"="Opaque" }

	// inside Pass
	ZWrite Off
	Blend SrcAlpha OneMinusSrcAlpha

    Pass {


      CGPROGRAM
      #pragma target 4.5

      #pragma vertex vert
      #pragma fragment frag

      #include "UnityCG.cginc"
      #include "Includes/Vert.cginc"
	  #include "Includes/FFT.cginc"

      uniform sampler2D _Texture;


      uniform float3 _Color;
      StructuredBuffer<Vert> _vertBuffer;
		  float _Progress;
      float _Size;
		    StructuredBuffer<FFT> _FFT;


      //A simple input struct for our pixel shader step containing a position.
      struct varyings {
          float4 pos 			: SV_POSITION;
          float2 uv  			: TEXCOORD1;
          float3 eye      : TEXCOORD5;
          float3 worldPos : TEXCOORD6;
          float debug    : TEXCOORD7;
      };

      varyings vert (uint id : SV_VertexID){

        varyings o;

        int bID = id / 6;

        int tri = id % 6;

       	Vert v = _vertBuffer[bID];
		float3 fPos = v.pos;

		float3 up = UNITY_MATRIX_IT_MV[0].xyz;
		float3 ri = UNITY_MATRIX_IT_MV[1].xyz;
		
		FFT fft = _FFT[(bID) % 128];
		
		float size = _Size + fft.data*0.00001;

        float2 fUV;

		if ( tri == 0 || tri == 5 ) { 
			fPos -= ri * size; 
			fUV = float2(0,0);
		}
		if( tri == 1 || tri == 4 )
		{ 
			fPos += ri * size; 
			fUV = float2(1,1);
		}
		if( tri == 2 )
		{ 
			fPos += up * size;
			fUV = float2(0,1);
		}
		if( tri == 3 )
		{ 
			fPos -= up * size; 
			fUV = float2(1,0);
		}

		o.pos = UnityObjectToClipPos (float4(fPos,1.0f));
		o.worldPos = fPos;
		o.eye = v.nor;
		o.debug = bID;
		o.uv = fUV;

        return o;


      }
      //Pixel function returns a solid color for each point.
      float4 frag (varyings v) : COLOR {

        float4 tCol = (float4(normalize((v.eye)),.0))*0.5+0.5;
		FFT fft = _FFT[(v.debug) % 128];
		tCol.rgb = pow(tCol, 1.+fft.data*.5);
		return tCol;


      }

      ENDCG

    }
  }

  Fallback Off
  
}